package com.iiht.capsule.taskmanager.controller;

import java.net.URI;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.iiht.capsule.taskmanager.model.Task;
import com.iiht.capsule.taskmanager.service.TaskManagerService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class TaskManagerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TaskManagerController.class);

	@Autowired
	private TaskManagerService taskManagerService;

	@RequestMapping("/hello")
	public String Tasks() {
		return "Hello";
	}

	@RequestMapping("/tasks")
	public List<Task> getAllTopics() {
		return taskManagerService.getAllTasks();
	}

	@RequestMapping("/tasks/{id}")
	public ResponseEntity<Object> getTask(@PathVariable Long id) {
		LOGGER.info("**********ID***********" + id);
		Task updatedTask = taskManagerService.getTask(id);

		if (updatedTask != null) {
			return ResponseEntity.ok(updatedTask);
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Task not found with ID " + id);

	}

	@PostMapping("/tasks")
	public ResponseEntity<Object> createTask(@RequestBody Task task) {
		LOGGER.info("********************" + task.toString());
		Task createdTask = taskManagerService.addTask(task);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(createdTask.getId()).toUri();

		return ResponseEntity.created(location).build();

	}

	@PutMapping("/tasks/{id}")
	public ResponseEntity<Object> updateTask(@RequestBody Task task, @PathVariable Long id) {
		LOGGER.info("*********Task***********" + task.toString());
		LOGGER.info("**********ID***********" + id);
		Task updatedTask = taskManagerService.updateTask(task, id);

		if (updatedTask != null) {
			return ResponseEntity.status(HttpStatus.OK).body("");
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Task not found with ID " + id);

	}

	@DeleteMapping("/tasks/{id}")
	public ResponseEntity<Object> deleteTask(@PathVariable Long id) {
		LOGGER.info("**********ID***********" + id);
		Task task = taskManagerService.getTask(id);

		if (task != null) {
			taskManagerService.deleteTask(id);

			return ResponseEntity.status(HttpStatus.OK).body("Task deleted successfully with ID " + id);
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Task not found with ID " + id);

	}

}
