package com.iiht.capsule.taskmanager.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.iiht.capsule.taskmanager.model.Task;

@Repository
public interface TaskManagerRepository extends CrudRepository<Task, Long> {

}
