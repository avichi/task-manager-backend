package com.iiht.capsule.taskmanager.service;

import java.util.List;

import com.iiht.capsule.taskmanager.model.Task;

public interface TaskManagerService {

	Task addTask(Task task);

	List<Task> getAllTasks();

	Task getTask(Long Id);

	Task updateTask(Task task, Long Id);

	void deleteTask(Long Id);
}
