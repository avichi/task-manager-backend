package com.iiht.capsule.taskmanager.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iiht.capsule.taskmanager.model.Task;
import com.iiht.capsule.taskmanager.repo.TaskManagerRepository;

@Service
public class TaskManagerServiceImpl implements TaskManagerService {

	@Autowired
	private TaskManagerRepository taskManagerRepository;

	@Override
	public Task addTask(Task task) {
		return taskManagerRepository.save(task);
	}

	@Override
	public List<Task> getAllTasks() {
		List<Task> tasks = new ArrayList<>();
		taskManagerRepository.findAll().forEach(tasks::add);
		return tasks;
	}

	@Override
	public Task updateTask(Task task, Long Id) {
		Task taskAvailable = taskManagerRepository.findById(Id).orElse(null);
		task.setId(Id);
		taskManagerRepository.save(task);
		return taskAvailable;
	}

	@Override
	public Task getTask(Long Id) {
		Task taskAvailable = taskManagerRepository.findById(Id).orElse(null);
		return taskAvailable;
	}

	@Override
	public void deleteTask(Long Id) {
		// TODO Auto-generated method stub

		taskManagerRepository.deleteById(Id);

	}

}
